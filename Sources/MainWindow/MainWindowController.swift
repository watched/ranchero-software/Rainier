//
//  MainWindowController.swift
//  Rainier
//
//  Created by Brent Simmons on 11/8/18.
//  Copyright © 2018 Ranchero Software, LLC. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

}
