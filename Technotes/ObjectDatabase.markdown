# The Object Database

Rainier’s object database — ODB, for short — is a schema-less, hierarchical key-value storage system.

Think of it as a dictionary that can contain dictionaries, and those dictionaries can contain dictionaries, etc.

The ODB calls those *tables* instead of dictionaries — *table* is short for *hashtable*. These tables should not be confused with tables in a SQL database.

Any individual table can contain any values and any sub-tables, since there is no schema.

### What It Looks Like to the Scripter

When writing a Rainier script, persistence is as easy as this:

	user.prefs.name = "Iggy Pop"

In the above, there’s a table named "user" which contains a table named “prefs” which contains a key "name" with a value "Iggy Pop." That line of code stores that value, and that value persists, on disk, between runs of the app.

(The `user.prefs.name` part is called, obviously enough, a *path*.)

The database can also store more complex forms of data: a script, an outline, binary data, and so on.

### Implementation

Before getting to the low-level storage, there are some things to consider.

#### In-memory Cache

There’s an in-memory cache of the parts of the database that have been referenced. Once a value in a table is read or set, the tables in that path would all be read into memory. Any unreferenced subtables would Core-Data-like faults (this way we don’t have to read in the entire database).

#### Non-Immediate Writing to Disk

The on-disk storage would not be updated immediately on every change: instead, the in-memory cache would be updated, and then changes would be written to disk periodically. Ideally this would happen once the system is otherwise idle, with some timeout so that an overly-active system wouldn’t prevent changes being written to disk.

For instance: there are times when a scripter might make changes to the database in a tight loop. We wouldn’t want to write to disk on each turn through the loop.

The best bet is probably to simplify this: just have an API for saving a database, and let the containing application decide when it’s the right time to save. The ODB would need to keep track of precisely what changes it needs to make on disk. (A value that changed 100 times would have only its current value saved: it wouldn’t replay all the changes.)

#### Primitive types and application types

Each object in the database is either a table or its some kind of value. Each value has two attributes: `primitiveType` and `applicationType``.

The `primitiveType` is something like string, boolean, binary data, array, etc. (If it’s an array, it can contain a list of values of any type.)

The `applicationType` is defined by the app using the object database. Rainier, for instance, would store scripts and other things in the database. A script’s `primitiveType` would be a string, and its `applicationType` would be script. Rainier understands what a script is, but the ODB only knows that its a string.

Converting between the value the ODB understands and the values the app understands is something the app handles: it’s not part of the ODB implementation. (This way the ODB is agnostic about the app that’s using it.)

### Low-level Storage

#### One possible way to go

This could all be done in SQLite. In fact, I got started on this in RSDatabase.framework — see the ODB code there.

Here’s the SQLite schema:

	CREATE TABLE if not EXISTS odb_tables (id INTEGER PRIMARY KEY AUTOINCREMENT, parent_id INTEGER NOT NULL, name TEXT NOT NULL);

	CREATE TABLE if not EXISTS odb_values (id INTEGER PRIMARY KEY AUTOINCREMENT, odb_table_id INTEGER NOT NULL, name TEXT NOT NULL, primitive_type INTEGER NOT NULL, application_type TEXT, value BLOB);

This schema allows for tables and values. A table has an id and the id of its parent table. It has a name.

Every value has an id, the id of its parent table `odb_table_id`, a name, a `primitiveType` and an `applicationType`, and a value (a BLOB, which means it can be anything).

Moving a value from one table to another is as simple as updating the `odb_table_id` of that value. Deleting a table would cascade to deleting its values and subtables via a SQL trigger.

Using SQLite for this would be great because we know SQLite is amazing. It’s fast and reliable. It means not having to actually write a database — this all just becomes an oddball front-end to SQLite.

#### Another way to go

The ODB is inspired by the ODB in UserLand Frontier. Frontier used Knuth’s boundary tag method. 

This meant treating the database as if were memory, but instead of locations in memory it was locations in a file on disk. In other words: it’s malloc and free but on disk.

I don’t know if this is the best explanation, but it’s what I found: [https://www.bradrodriguez.com/papers/ms/pat4th-c.html](https://www.bradrodriguez.com/papers/ms/pat4th-c.html)

This has the potential of being able to store things more efficiently than SQLite would. Especially when combined with the idea that a table on disk would probably store its smaller values in its block, and store only larger values (>2K, something like that) in separate blocks.

Using this method would mean a lot more low-level code, though, and it’s hard to imagine making it as reliable as SQLite. Database corruption was a real problem with Frontier: it was never as bulletproof as it needed to be.

This makes me lean toward using SQLite, but if I can be persuaded that this could be made as reliable as SQLite, I’d go with it for its greater efficiency.